import numpy as np
import matplotlib.pyplot as plt
import os
from nptdms import TdmsFile
from scipy.signal import butter,sosfilt, spectrogram, periodogram
from ipywidgets import interactive, IntSlider, IntRangeSlider
from IPython.display import display


def read_patient(extraChannel: bool, patient_number_int: int):
    """
    Reads the tdms file of a specific patient
    :param extraChannel: Boolean, specifies whether or not to read the extra channel tdms
    :param patient_number_int: Integer, patient identifier, e.g., '44' 
    """

    if patient_number_int < 100:
        patient_number_str = f"P0{patient_number_int}"
    else:
        patient_number_str = f"P{patient_number_int}"

    directory_path = f"./patients/{patient_number_str}"
     # Get all TDMS file in the directory
    tdms_files = [f for f in os.listdir(directory_path) if f.endswith('.tdms')]
    if not tdms_files:
        raise FileNotFoundError(f"No TDMS files found for patient {patient_number_str} in {directory_path}")

    if extraChannel:
        file_name = max(tdms_files)
    else:
        file_name = min(tdms_files)
    

    file_path = os.path.join(directory_path, file_name)

    # Read the TDMS file
    tdms_file = TdmsFile.read(file_path)
    return tdms_file

def read_patient_channel_data(patient_number_int: int, group_number: int, extraChannel: bool = False):
    """
    Reads data from a specific channel and group within a TDMS file for a given patient.

    :param patient_number_int: Integer, patient identifier, e.g., '44'
    :param group_number: Integer, group identifier between 0-3, 0 means the Empty group
    :return: array num_samples x channels
    """
   
    tdms_file = read_patient(extraChannel, patient_number_int)

    scaling=2.7e-3 # V/pT  from 2.7 #V/nT

    data_dict={}

    group=tdms_file.groups()[group_number]
    for i,channel in enumerate(group.channels()):
        # Access dictionary of properties:
        properties = channel.properties
        #print(properties)
        # Access numpy array of data for channel:
        data_dict[i] = channel.read_data(scaled=True)
    no_of_channels=len(data_dict)
    #Create common time axis using the scaling from the file
    data_dict[-1]=np.linspace(0,(len(data_dict[0])-1)*properties['wf_increment'],len(data_dict[0]))
    fs=1/data_dict[-1][1]

    #Create an array with the content as the dict:
    data_array=np.zeros((len(data_dict[-1]),no_of_channels+1))
    data_array[:,0]=data_dict[-1]

    for i in range(no_of_channels):
        data_array[:,i+1]=data_dict[i]/scaling
    return data_array

def butter_filter(dd, lpfc=100, lpo=2, bsfc=[49.5, 50.5], bso=3):
    #Filter data
    ''' 
        dd: channel data
        lpfc: the critical frequency for lowpass filter
        lpo: order of the lowpass filter
        bsfc: the critical frequency for lowpass filter
        bstfc: bandstop important frequencies 
        bso: order of the lowpass filter
        Freqencytoremove: Frequency to remove for notch filter
        '''
    fs=1000
    band_stop = butter(bso, bsfc, btype='bandstop',output='sos',fs=fs)
    lowpass = butter(lpo,lpfc,btype='low',output='sos',fs=fs)
    for i in range(1,np.shape(dd)[1]):
        dd[:,i]=sosfilt(band_stop,dd[:,i])
        dd[:,i]=sosfilt(lowpass,dd[:,i])
    return dd

def plot_compare(raw_data, filtered_data, channel, fs = 1000):
    # Constants
    fs = 1000  # Sampling frequency in Hz

    # Calculate time axis for the data
    t = np.arange(len(raw_data[channel])) / fs  # Time vector from samples

    # Plot raw and filtered data
    plt.figure(figsize=(14, 6))

    # Plotting Raw Data
    plt.subplot(1, 2, 1)
    plt.plot(t, raw_data[channel], label='Raw Data')
    plt.title('Raw Data')
    plt.xlabel('Time (seconds)')
    plt.ylabel('Amplitude')
    plt.legend()

    # Plotting Filtered Data
    plt.subplot(1, 2, 2)
    plt.plot(t, filtered_data[channel], label='Filtered Data', color='red')
    plt.title('Filtered Data')
    plt.xlabel('Time (seconds)')
    plt.ylabel('Amplitude')
    plt.legend()

    plt.tight_layout()
    plt.show()

def interactive_matrix_plotter(data, filtered_data, fs):
    def plot_data(channel=0, sample_range=(0, 2*fs)):
        fig, (ax1, ax2) = plt.subplots(1, 2, figsize=(15, 5))
        
        # Plot raw data
        ax1.plot(data[sample_range[0]:sample_range[1], channel])
        ax1.set_title(f"Raw Data - Channel {channel + 1}")
        ax1.set_xlabel("Sample Index")
        ax1.set_ylabel("Amplitude")
        ax1.grid(True)

        # Plot filtered data
        ax2.plot(filtered_data[sample_range[0]:sample_range[1], channel])
        ax2.set_title(f"Filtered Data - Channel {channel + 1}")
        ax2.set_xlabel("Sample Index")
        ax2.set_ylabel("Amplitude")
        ax2.grid(True)

        plt.tight_layout()
        plt.show()

    # Create interactive widgets
    channel_slider = IntSlider(value=0, min=0, max=data.shape[1] - 1, step=1, description='Channel')
    sample_range_slider = IntRangeSlider(
        value=[0, 2*fs],
        min=0, 
        max=data.shape[0] - 1,
        step=fs,
        description='Sample Range'
    )

    # Create the interactive visualization
    interactive_plot = interactive(plot_data, channel=channel_slider, sample_range=sample_range_slider)

    # Display the interactive widgets and plot
    display(interactive_plot)

def show_all_channels(data, start, duration):
    plt.figure(figsize=(5, 40))  # Adjust the width and height as needed

    # Loop over each component
    for i in range(data.shape[1]):  # Assuming you have 18 components based on your question
        plt.subplot(data.shape[1], 1, i + 1)  # 18 rows, 1 column, i-th subplot
        plt.plot(data[start:(start + duration)][:, i])  # Plotting the i-th component
        plt.title(f'Component {i + 1}')
        plt.xlabel('Time (s)')  # Assuming time units are seconds, adjust if different
        plt.tight_layout()  # Adjust layout to prevent label/title overlap

    # Show the plot
    plt.show()

def compare_all_channels(data, filtered_data, start, duration):
    plt.figure(figsize=(10, 80))  # Adjust the width to accommodate 2 columns
    plt.title(f'Raw - Filtered')
    # Loop over each component
    for i in range(data.shape[1]):  # Assuming data.shape[1] gives you 18 components
        # Plot the original data
        plt.subplot(data.shape[1], 2, 2*i + 1)  # 18 rows, 2 columns, odd indices for original data
        plt.plot(data[start:(start + duration)][:, i])
        plt.xlabel('Time (s)')  # Assuming time units are seconds, adjust if different
        plt.tight_layout()
        # Plot the filtered data
        plt.subplot(data.shape[1], 2, 2*i + 2)  # 18 rows, 2 columns, even indices for filtered data
        plt.plot(filtered_data[start:(start + duration)][:, i])
        plt.xlabel('Time (s)')
        plt.tight_layout()
    plt.show() 

def interactive_periodogram(data, filtered_data, fs = 1000):
    # Number of channels
    n_channels = data.shape[1]
    
    # Pre-compute periodograms for all channels
    raw_periodograms = []
    filtered_periodograms = []
    for ch in range(n_channels):
        f_raw, pxx_raw = periodogram(data[:, ch], fs=fs, window='hamming')
        f_filtered, pxx_filtered = periodogram(filtered_data[:, ch], fs=fs, window='hamming')
        raw_periodograms.append((f_raw, pxx_raw))
        filtered_periodograms.append((f_filtered, pxx_filtered))

    def plot_data(channel=0, freq_range=(0, fs//2)):
        min_freq, max_freq = freq_range
        f_raw, pxx_raw = raw_periodograms[channel]
        f_filtered, pxx_filtered = filtered_periodograms[channel]

        # Limit frequencies
        idx_raw = (f_raw >= min_freq) & (f_raw <= max_freq)
        idx_filtered = (f_filtered >= min_freq) & (f_filtered <= max_freq)

        plt.figure(figsize=(14, 6))

        # Plotting raw data periodogram
        plt.subplot(1, 2, 1)
        plt.loglog(f_raw[idx_raw], pxx_raw[idx_raw], label='Raw Data')
        plt.xlabel('Frequency (Hz)')
        plt.ylabel('Power Spectral Density (dB/Hz)')
        plt.title(f'Periodogram of Raw Data ({min_freq}-{max_freq} Hz)')
        plt.legend()

        # Plotting filtered data periodogram
        plt.subplot(1, 2, 2)
        plt.loglog(f_filtered[idx_filtered], pxx_filtered[idx_filtered], color='red', label='Filtered Data')
        plt.xlabel('Frequency (Hz)')
        plt.ylabel('Power Spectral Density (dB/Hz)')
        plt.title(f'Periodogram of Filtered Data ({min_freq}-{max_freq} Hz)')
        plt.legend()

        plt.tight_layout()
        plt.show()

    # Create interactive widgets
    channel_slider = IntSlider(value=0, min=0, max=n_channels - 1, step=1, description='Channel')
    freq_range_slider = IntRangeSlider(value=[0, fs//2], min=0, max=fs//2, step=1, description='Frequency Range')

    # Create the interactive visualization
    interactive_plot = interactive(plot_data, channel=channel_slider, freq_range=freq_range_slider)

    # Display the interactive widgets and plot
    display(interactive_plot)

def interactive_spectrogram(data, filtered_data, fs = 1000):
    # Number of channels
    n_channels = data.shape[1]
    
    # Pre-compute spectrograms for all channels
    raw_spectrograms = []
    filtered_spectrograms = []
    for ch in range(n_channels):
        f_raw, t_raw, Sxx_raw = spectrogram(data[:, ch], fs=fs, window='hamming', nperseg=1024)
        f_filtered, t_filtered, Sxx_filtered = spectrogram(filtered_data[:, ch], fs=fs, window='hamming', nperseg=1024)
        raw_spectrograms.append((f_raw, t_raw, Sxx_raw))
        filtered_spectrograms.append((f_filtered, t_filtered, Sxx_filtered))

    def plot_data(channel=0, freq_range=(0, fs//2)):
        min_freq, max_freq = freq_range
        f_raw, t_raw, Sxx_raw = raw_spectrograms[channel]
        f_filtered, t_filtered, Sxx_filtered = filtered_spectrograms[channel]

        # Limit frequencies
        idx_raw = (f_raw >= min_freq) & (f_raw <= max_freq)
        idx_filtered = (f_filtered >= min_freq) & (f_filtered <= max_freq)

        plt.figure(figsize=(14, 6))

        # Plotting raw data spectrogram
        plt.subplot(1, 2, 1)
        plt.pcolormesh(t_raw, f_raw[idx_raw], 10 * np.log10(Sxx_raw[idx_raw, :]), shading='gouraud')
        plt.xlabel('Time (s)')
        plt.ylabel('Frequency (Hz)')
        plt.title(f'Spectrogram of Raw Data ({min_freq}-{max_freq} Hz)')
        plt.colorbar(label='Decibels (dB)')
        plt.ylim(min_freq, max_freq)

        # Plotting filtered data spectrogram
        plt.subplot(1, 2, 2)
        plt.pcolormesh(t_filtered, f_filtered[idx_filtered], 10 * np.log10(Sxx_filtered[idx_filtered, :]), shading='gouraud')
        plt.xlabel('Time (s)')
        plt.ylabel('Frequency (Hz)')
        plt.title(f'Spectrogram of Filtered Data ({min_freq}-{max_freq} Hz)')
        plt.colorbar(label='Decibels (dB)')
        plt.ylim(min_freq, max_freq)

        plt.tight_layout()
        plt.show()

    # Create interactive widgets
    channel_slider = IntSlider(value=0, min=0, max=n_channels - 1, step=1, description='Channel')
    freq_range_slider = IntRangeSlider(value=[0, fs//2], min=0, max=fs//2, step=1, description='Frequency Range')

    # Create the interactive visualization
    interactive_plot = interactive(plot_data, channel=channel_slider, freq_range=freq_range_slider)

    # Display the interactive widgets and plot
    display(interactive_plot)

